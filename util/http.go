package util

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

// Types

// For monkey-patching in mocked http clients
type IHttpClient interface {
	Get(string) (*http.Response, error)
	Post(string, string, io.Reader) (*http.Response, error)
}

// End Types

var HttpClientBase IHttpClient = &http.Client{
	Timeout: 10 * time.Second,
}

func HttpClientFactory() IHttpClient {
	client := HttpClientBase
	return client
}

func ReadJSONBody(
	req *http.Request,
	v interface{},
) error {
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return err
	}

	err = json.Unmarshal(body, v)
	return err
}
