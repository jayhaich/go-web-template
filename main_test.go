package main

import (
	"context"
	"flag"
	"os"
	"testing"
	"time"

	log "github.com/sirupsen/logrus"
)

func TestMain(t *testing.T) {
	origArgs := os.Args
	os.Args = []string{
		os.Args[0],
		"-config",
		"./test_data/test_config.json",
	}
	origCommandLine := flag.CommandLine
	// prevent a 'flag redefined' error
	// flag.CommandLine = flag.NewFlagSet(os.Args[0], flag.ExitOnError)
	// setMockHttpClient()

	defer func() {
		os.Args = origArgs
		flag.CommandLine = origCommandLine
	}()

	go func() {
		defer func() {
			if r := recover(); r != nil {
				t.Errorf("TestMain panicked: '%v'", r)
			}
		}()
		log.Println("Running main()")
		main()
		log.Println("Finished running main()")
	}()
	time.Sleep(1 * time.Second)
	err := HttpServer.Shutdown(context.Background())
	if err != nil {
		t.Error(err)
	}
}
