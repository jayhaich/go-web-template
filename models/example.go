package models

import (
	log "github.com/sirupsen/logrus"
)

type Example struct {
	Foo string
	Bar bool
	Baz int64
}

func (e *Example) Validate() bool {
	isValid := true

	if e.Baz == 0 {
		log.Println("You forgot to Baz")
		isValid = false
	}
	return isValid
}
