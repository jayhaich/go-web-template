#!/usr/bin/make -f

PREFIX ?= /opt/$(BIN)
NAME ?= myexe
BIN ?= $(NAME)-server
DESTDIR ?=
INSTDIR = $(DESTDIR)/$(PREFIX)
BINDIR = $(INSTDIR)/bin
ETCDIR = $(INSTDIR)/etc
LOGDIR ?= $(INSTDIR)/log
# Alternately replace with podman or other compatible container engine
DOCKER ?= docker

all:
	go build -o $(BIN)

clean:
	rm -f $(BIN) *.rpm coverage.out

docker:
	$(DOCKER) build -t $(NAME) .

install:
	install -d $(BINDIR)
	install -d $(ETCDIR)
	install -d $(DESTDIR)/etc/$(NAME)
	install -d $(DESTDIR)/etc/logrotate.d
	install -d $(DESTDIR)/usr/lib/systemd/system
	install -d $(LOGDIR)
	install -m 0755 $(BIN) $(BINDIR)
	install -m 0644 files/prod-config.json $(DESTDIR)/etc/$(NAME)
	install -m 0644 files/logrotate $(DESTDIR)/etc/logrotate.d/$(NAME)
	install -m 0644 files/systemd.service \
		$(DESTDIR)/usr/lib/systemd/system/$(NAME).service

rpm:
	$(eval version := $(shell jq .version meta.json))
	$(eval release := $(shell jq .release meta.json))
	rpmdev-setuptree
	rm -rf ~/rpmbuild/BUILD/$(NAME)*
	rm -rf ~/rpmbuild/RPMS/$(NAME)*
	cp -r . ~/rpmbuild/BUILD/$(NAME)-$(version)
	rpmbuild -ba $(NAME).spec \
		-D "version $(version)" \
		-D "release $(release)"
	cp ~/rpmbuild/RPMS/x86_64/$(NAME)-$(version)-$(release).x86_64.rpm .

test:
	./run_tests.sh
