package config

import (
	"os"
	"testing"
	"time"
)

func TestLoadConfig(t *testing.T) {
	LoadConfig("../test_data/test_config.json")
}

func TestConfigValidate(t *testing.T) {
	go func() {
		defer func() {
			if r := recover(); r == nil {
				t.Error("TestConfigValidate did not panic")
			}
		}()
		config := LoadConfig("../test_data/bad_config.json")
		config.Validate()
	}()
	time.Sleep(1 * time.Second)
}

func TestConfigEnvVarOverride(t *testing.T) {
	dbName := "TestConfigEnvVarOverride"
	os.Setenv("DATABASE_NAME", dbName)
	config := LoadConfig("../test_data/bad_config.json")
	if config.Database != dbName {
		t.Error(
			"TestConfigEnvVarOverride value is",
			config.Database,
			"not",
			dbName,
		)
	}
}
