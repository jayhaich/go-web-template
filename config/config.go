package config

import (
	"encoding/json"
	"io/ioutil"
	"os"

	log "github.com/sirupsen/logrus"
	"gitlab.com/jaelen/go-web-template/util"
)

type Config struct {
	// The name of the MongoDB database to use
	Database string
	// The server:port address to connect to MongoDB
	MongoAddress string
	// The HTTP server:port address to serve on
	ServeAddress string
	// The optional static directory to serve files from
	StaticDir string
}

var Cfg *Config = nil

func (c *Config) Validate() {
	valid := true

	if c.Database == "" {
		log.Println("Config Error: 'Database' is empty")
		valid = false
	}
	if c.MongoAddress == "" {
		log.Println("Config Error: 'MongoAddress' is empty")
		valid = false
	}
	if c.ServeAddress == "" {
		log.Println("Config Error: 'ServeAddress' is empty")
		valid = false
	}

	if c.StaticDir != "" && !util.FileExists(c.StaticDir) {
		log.Printf("Static dir '%v' does not exist\n", c.StaticDir)
		valid = false
	}

	if !valid {
		panic("Invalid config")
	}
}

func LoadConfig(path string) Config {
	var b []byte
	var config Config

	if _, err := os.Stat(path); err == nil {
		b, err = ioutil.ReadFile(path)
		util.FatalError(err)
		err = json.Unmarshal(b, &config)
		util.FatalError(err)
	}

	for k, v := range map[string]*string{
		"DATABASE_NAME": &config.Database,
		"MONGO_ADDRESS": &config.MongoAddress,
		"SERVE_ADDRESS": &config.ServeAddress,
		"STATIC_DIR": &config.StaticDir,
	}{
		if val, isSet := os.LookupEnv(k); isSet {
			log.Printf(
				"Env. var. %s set, overriding '%s' with '%s'",
                                k,
				*v,
				val,
			)
			*v = val
		}
	}

	return config
}
