package routes

import (
	"bytes"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"gitlab.com/jaelen/go-web-template/config"
	"gitlab.com/jaelen/go-web-template/util"
)

type ResponseFactory struct {
	StatusCode int
	Path       string
}

func (r *ResponseFactory) create() *http.Response {
	var file *os.File = nil
	var err error
	if r.Path != "" {
		file, err = os.Open(r.Path)
		if err != nil {
			panic(err)
		}
	}
	return &http.Response{
		StatusCode: r.StatusCode,
		Body:       file,
	}
}

type MockHttpClient struct {
	getResp  ResponseFactory
	postResp ResponseFactory
}

func (c *MockHttpClient) Get(url string) (*http.Response, error) {
	return c.getResp.create(), nil
}

func (c *MockHttpClient) Post(
	url string,
	contentType string,
	body io.Reader,
) (
	*http.Response,
	error,
) {
	return c.postResp.create(), nil
}

func setMockHttpClient(client *MockHttpClient) {
	util.HttpClientBase = client
}

type validateFail struct{}

func (v validateFail) Validate() bool {
	return false
}

func TestValidateAndInsertValidateFail(t *testing.T) {
	cfg := config.LoadConfig("../test_data/test_config.json")
	config.Cfg = &cfg
	w := httptest.NewRecorder()
	v := validateFail{}
	r, _ := http.NewRequest(
		"POST",
		"/something",
		bytes.NewBuffer([]byte(`{"some_key": "some_value"}`)),
	)
	err := ValidateAndInsert(
		w,
		r,
		v,
		"TestValidateAndInsertValidateFail",
	)
	if err == nil {
		t.Error("err was nil, should not have been")
	}
}

func TestValidateAndInsertMongoFail(t *testing.T) {
	cfg := config.LoadConfig("../test_data/test_config.json")
	config.Cfg = &cfg
	config.Cfg.MongoAddress = ":23456"
	w := httptest.NewRecorder()
	v := validateFail{}
	r, _ := http.NewRequest(
		"POST",
		"/something",
		bytes.NewBuffer([]byte(`{"some_key": "some_value"}`)),
	)
	err := ValidateAndInsert(
		w,
		r,
		v,
		"TestValidateAndInsertMongoFail",
	)
	if err == nil {
		t.Error("err was nil, should not have been")
	}
}
