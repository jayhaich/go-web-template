package routes

import (
	"net/http"

	"gitlab.com/jaelen/go-web-template/models"
)

func ExampleRoute(
	w http.ResponseWriter,
	r *http.Request,
) {
	logRequest(r)
	switch r.Method {
	case "GET":
		exampleRouteGet(w, r)
	case "POST":
		exampleRoutePost(w, r)
	default:
		badRequestInvalidMethod(w, r)
	}
}

func exampleRouteGet(
	w http.ResponseWriter,
	r *http.Request,
) {
	// do something
	w.WriteHeader(http.StatusOK)
}

func exampleRoutePost(
	w http.ResponseWriter,
	r *http.Request,
) {
	ValidateAndInsert(
		w,
		r,
		&models.Example{},
		"example",
	)
}
