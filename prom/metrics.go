package prom

/* Prometheus metrics
 */

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	FooFailureTotal = promauto.NewCounter(prometheus.CounterOpts{
		Name: "myexe_foo_failure_total",
		Help: "The total number of foo failures",
	})
	BarFailureTotal = promauto.NewCounter(prometheus.CounterOpts{
		Name: "myexe_bar_failure_total",
		Help: "The total number of bar failures",
	})
)
