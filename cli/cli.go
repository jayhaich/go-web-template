package cli

import (
	"flag"
	"os"
)

var Config *string

/* Defines and parses the command line arguments
 */
func Parse() {
	defaultConfig := "./files/config.json"
	if val, isSet := os.LookupEnv("CONFIG_PATH"); isSet {
		defaultConfig = val
	}
	Config = flag.String(
		"config",
		defaultConfig,
		"The path to the configuration file in JSON format",
	)
	flag.Parse()
}

/* Validates the the command line arguments are sane
 * Call after Parse()
 */
func Validate() {
	// Add validations here
}
