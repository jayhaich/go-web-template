package cli

import (
	"os"
	"testing"
)

func TestConfigParse(t *testing.T) {
	configPath := "testing"
	os.Setenv("CONFIG_PATH", configPath)
	Parse()
	if *Config != configPath {
		t.Errorf(
			"Was '%s', expected '%s'",
			*Config,
			configPath,
		)
	}
}

func TestConfigValidate(t *testing.T) {
	Validate()
}
