package mongo

import (
	"gopkg.in/mgo.v2"

	"gitlab.com/jaelen/go-web-template/config"
)

func EnsureIndices() {
	session := Session()
	defer session.Close()

	zc := session.DB(config.Cfg.Database).C("my_collection")
	zc.EnsureIndex(
		mgo.Index{
			Key: []string{"my_key"},
		},
	)
}
